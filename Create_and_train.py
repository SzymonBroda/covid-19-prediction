import pandas as pd
import tensorflow as tf
from sklearn.utils import shuffle
import numpy as np
import random
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

#nieużywane funkcje normalizacji danych
'''
def normalize_by_column(df):
    for col in df.columns:
        df[col] /= df[col].max()
        df.dropna(inplace=True)
    return df


def normalize_by_df(df):
    max_value = 0
    for col in df.columns:
        if df[col].max() > max_value:
            max_value = df[col].max()

    for col in df.columns:
        df[col] /= max_value
        df.dropna(inplace=True)
    return df
'''


#wybieranie miast dla których dane są dłuższe niż DAYS(40dni)
def select_cities(df):
    temp_df = df['Province/State'].value_counts()
    cities = temp_df[temp_df > DAYS].axes
    return cities[0]


#podział danych na zbiory oraz ich odpowiednia spreparowanie
def preprocess_data(df, history_size, TRAIN_SIZE, VALID_SIZE ):
    data = []
    x_data = []
    y_data = []
    history_size = history_size + 1

    for column in df:
        for i in range(history_size, df[column].size-1):
            values = df[column][i-history_size:i].values
            data.append(values)

    random.shuffle(data)

    for i in range(len(data)):
        x_data.append(data[i][:-1])
        y_data.append(data[i][-1])


    train_size = int(TRAIN_SIZE * len(x_data))
    valid_size = int(VALID_SIZE * len(x_data))

    x_train = x_data[:train_size]
    y_train = y_data[:train_size]
    x_valid = x_data[train_size:train_size + valid_size]
    y_valid = y_data[train_size:train_size + valid_size]
    x_test = x_data[train_size + valid_size:]
    y_test = y_data[train_size + valid_size:]

    return np.array(x_train), np.array(y_train), \
           np.array(x_valid), np.array(y_valid), \
           np.array(x_test), np.array(y_test),


#wykreslanie wynikow
def show_plot(history, true_future, prediction):
    time_steps = np.arange(0, HISTORY_DATA, 1)
    plt.title('Sample Example')
    plt.plot(time_steps, history, marker='o', label='History')
    plt.plot(HISTORY_DATA, true_future, marker='x', label='True Future')
    plt.plot(HISTORY_DATA, prediction, marker='*', label='Prediction')
    plt.legend()
    plt.xlabel('DAYS')
    plt.show()

my_df = pd.read_csv("covid_19_data.csv")
my_df = my_df[['Province/State', 'Confirmed']]#, 'Deaths', 'Recovered']]

DAYS = 40
cities = select_cities(my_df)
covid_df = pd.DataFrame()

for city in cities:
    values_to_add = my_df[city == my_df['Province/State']][:DAYS]
    covid_df['{}'.format(city)] = values_to_add['Confirmed'].values

del covid_df['Diamond Princess cruise ship'] #usuwanie wadliwego zbioru danych
#norm_df = normalize_by_df(covid_df.copy())


HISTORY_DATA = 10 #siec neuronowa pracuje na danych z ostatnich 10 dni
TRAIN_SIZE = 0.8
VALID_SIZE = 0.1

x_train, y_train, x_valid, y_valid, x_test, y_test = preprocess_data(covid_df, HISTORY_DATA, TRAIN_SIZE, VALID_SIZE)

''' -------------------------------------------------------------------
# w wypadku, gdy pojawi się konieczność przeprowadzenia testow należy
# 1. zakomentować  'tworzenie sieci'
# 2. odkomentować 'część testową'
------------------------------------------------------------------------'''


#=====================================================
#tworzenie sieci
''''''
model = Sequential([
    Dense(x_train.shape[1], activation='relu'),
    Dense(64, activation='relu'),
    Dense(32, activation='relu'),
    Dense(16, activation='relu'),
    Dense(1, activation='relu')])

model.compile(optimizer='adam', loss='mae', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=10, epochs=1000, validation_data=(x_valid, y_valid))
model.save('covid_19_model_copy.h5')
''''''

#========================================================
#część testowa
'''
model = tf.keras.models.load_model('covid_19_model.h5')

y_pred = model.predict(x_test).astype(int)
for i in range(y_pred.shape[0]):
    show_plot(x_test[i], y_test[i], y_pred[i])
'''








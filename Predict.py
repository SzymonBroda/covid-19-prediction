import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


HISTORY = 10 #sieć neuronowa pracuje na danych z ostatnich 10 dni
model = tf.keras.models.load_model('covid_19_model.h5')


#wykreslanie wynikow
def show_plot(history, prediction, legnth, title):
    history_steps = np.arange(0, HISTORY, 1)
    predicted_steps = np.arange(HISTORY, legnth + HISTORY, 1)
    plt.title('Potwierdzone przypdaki - {}'.format(title))
    plt.plot(history_steps, history, marker='o', label='Wprowadzone dane')
    plt.plot(predicted_steps, prediction, marker='*', label='Przewidywania')
    plt.legend()
    plt.xlabel('Dni')
    plt.ylabel('Liczba osob zarazonych')
    plt.show()


#funckja odpowiadajaca za realizacje predykcji
def main_function(data, model, title):
    data_copy = data.copy()
    predicted = []
    print("Jak długiej predykcji oczekujesz?"
          "(sugerowane liczby dni - 50, 100, 300)")
    length = int(input())

    for i in range(length):
        y_pred = model.predict(data).astype(int)
        predicted = np.append(predicted, y_pred[0])
        data = data[0][1:]
        data = np.append(data, y_pred[0])
        data = np.reshape(data, (1, HISTORY))

    show_plot(data_copy[0], predicted, length, title)

print("Witaj w programie Coronavirus, aby przeprowadzić przewdidywania postępowania "
      "epidemi postępuj zgodnie z instrukcją.\n"
      "\nWybierz program:"
      "\n1. Wprowadz własne dane"
      "\n2. Użyj danych z województwa pomorskiego dla 10 ostatnich (stan na dzień 23.03)\n")
choice = input()

if choice == '1':
    entered_data = []
    print("Wprowadz dane dla kolejnych 10 dni")
    for i in range (HISTORY):
        print("dzien {}:".format(i))
        entered_data = np.append(entered_data, int(input()))

    entered_data = np.reshape(entered_data, (1, HISTORY))
    main_function(entered_data, model, "Twoje dane")


elif choice == '2':
    example_data = np.array([[2, 2, 2, 8, 9, 9, 10, 14, 17, 21]], dtype=np.float64)
    main_function(example_data, model, "Pomorze")
